# Go Templ + HTMX + Tailwind Template

A minimal template to get started using [Go Templ](https://templ.guide/) + [HTMX](https://htmx.org/) + [Tailwind](https://tailwindcss.com/)

NOTE: This is not a complete example of what can be done with these libraries. For that, consult the docs, or find real projects using these tools

## Running

```sh
go install github.com/a-h/templ/cmd/templ@latest
go install go install github.com/cosmtrek/air@latest
npx install -g tailwindcss
air
```
