package main

import (
	"fmt"
	"log"
	"net/http"
	"project/templates"
)

func main() {
	count := 0

	http.Handle("/public/", http.StripPrefix("/public/",
		http.FileServer(http.Dir("./public"))))

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		templates.IndexPage(count).Render(r.Context(), w)
	})

	http.HandleFunc("/count", func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case "GET":
			w.Write([]byte(fmt.Sprint(count)))
		case "POST":
			count++
			w.Write([]byte(fmt.Sprint(count)))
		}
	})

	log.Println("Listening on 5000")
	http.ListenAndServe(":5000", nil)
}
